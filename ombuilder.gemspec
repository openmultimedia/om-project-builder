($LOAD_PATH << File.expand_path("../lib", __FILE__)).uniq!

require 'ombuilder/version'

Gem::Specification.new do |spec|
  spec.name = 'ombuilder'
  spec.summary = 'OpenMultimedia project building library for Rake'
  spec.description = 'OpenMultimedia project building library for rake'

  begin
    spec.add_dependency "rake", "~>10"
    spec.add_dependency "haml", "~>4"
    #spec.add_dependency "css_parser"
    spec.add_dependency "csspool"
    spec.add_dependency "sass", "~>3"
    spec.add_dependency "compass", "~>0.12"
    spec.add_dependency "sinatra", "~>1.4"

    spec.add_development_dependency "rspec"
    spec.add_development_dependency "simplecov"

    file_patterns = [
      "lib/**/*.rb",
      "spec/**/*.rb",
      ".omversion",
      "Gemfile",
      "README.md",
      "Rakefile"
    ]

    spec.files = file_patterns.collect do |pattern|
      Dir.glob(pattern).sort
    end

    spec.files.flatten!

  end

  spec.author = 'Alan Reyes'
  spec.email = 'kg.designer@openmultimedia.biz'
  spec.version = OMBuilder::VERSION
  spec.homepage =  'http://openmultimedia.biz'
end
