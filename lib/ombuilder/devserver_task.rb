require 'rake'
require 'ombuilder/util/devserver'

module OMBuilder
  class DevServerTask < Rake::TaskLib
    attr_reader :server
    attr_reader :rake_task

    def initialize(&block)
      @server = Class.new(OMBuilder::StaticServer)

      @server.instance_eval(&block) if block_given?

      @rake_task = task(:devserver) { |t| @server.run! }
    end
  end
end
