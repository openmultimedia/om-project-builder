# coding: utf-8
require "rake"
require "ombuilder"
require "ombuilder/project_base"
require "ombuilder/project_configuration"
require 'ombuilder/project_version'

module OMBuilder
  class Project < Rake::TaskLib
    include ProjectBase

    attr_reader :version
    attr_reader :configurations

    def configuration_class
      ProjectConfiguration
    end

    def initialize(name, &block)
      @version = OMBuilder::ProjectVersion.new
      @configurations = Hash.new
      
      super name, &block
    end

    def add_configuration(configuration_name, &block)
      @configurations[configuration_name] = 
        configuration_class.new(configuration_name, self, &block)

      task configuration_name => @configurations[configuration_name][:default]
    end
  end
end