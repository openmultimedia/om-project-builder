require "pathname"
require "fileutils"
require 'ombuilder'

module OMBuilder
  module ClosureBuilder
    include OMBuilder::BuilderBase unless include? OMBuilder::BuilderBase

    class << self
      include ClosureBuilder

      def closure_tools_path
        @closure_tools_path ||= './closure-tools'
      end
    end

    attr_writer :using_closure_library
    def using_closure_library?
      @using_closure_library
    end

    attr_writer :using_closure_templates
    def using_closure_templates?
      @using_closure_templates
    end

    def closure_tools_path=(value)
      @closure_tools_path = relative_path_to value
    end

    def closure_tools_path
      @closure_tools_path || OMBuilder::ClosureBuilder.closure_tools_path
    end

    attr_writer :closure_library_path
    
    def closure_library_path
      @closure_library_path || File.join(closure_tools_path, 'closure-library')
    end

    def closure_library_root_path
      File.join closure_library_path, "closure/goog/"
    end

    def closure_library_third_party_root_path
      File.join closure_library_path, "third_party/closure/goog/"
    end

    def closure_library_base_js_path
      File.join closure_library_root_path, "base.js"
    end

    attr_writer :closure_compiler_jar

    def closure_compiler_jar
      @closure_compiler_jar || File.join(closure_tools_path, 'closure-compiler/compiler.jar')
    end

    def closure_stylesheets_jar
      File.join(closure_tools_path, 'closure-stylesheets/closure-stylesheets.jar')
    end

    def closure_templates_jar
      File.join(closure_tools_path, 'closure-templates/SoyToJsSrcCompiler.jar')
    end

    def closure_templates_msg_extractor_jar
      File.join(closure_tools_path, 'closure-templates-msg-extractor/SoyMsgExtractor.jar')
    end

    def closure_templates_root_path
      @closure_templates_root_path || File.join(closure_tools_path, 'closure-templates/')
    end

    def closure_deps(params)
      roots =  params[:roots] || []
      files =  params[:files] || []

      if (!roots or roots.length == 0) and (!files or files.length ==0)
        raise(ArgumentError, "roots or files parameter required")
      end

      output_file =  params[:output_file] || raise(ArgumentError, "output_file parameter required")

      if not roots.kind_of? Enumerable and not roots.kind_of? FileList then
        roots = [ roots.to_s ]
      end

      command = [ "python", "#{closure_library_path}/closure/bin/build/depswriter.py" ]

      roots.each do |path|
        relative_path = relative_path_to(path, closure_library_base_js_path)
        command << "--root_with_prefix" << "'#{path}' '#{relative_path}'";
      end

      files.each do |path|
        relative_path = relative_path_to(path, closure_library_base_js_path)
        command << "--path_with_depspath" << "'#{path}' '#{relative_path}'";
      end

      command << "--output_file" << output_file

      when_writing "Running Depswriter" do
        system(*command)
      end
    end

    alias closure_build_dependencies closure_deps

    def closure_templates_build(params)
      source = params[:source] || raise(ArgumentError, "source parameter required")
      output_path_format = params[:output_path_format] || raise(ArgumentError, "output_path_format parameter required")
      should_generate_jsdoc = params[:should_generate_jsdoc] || false
      should_provide_require_soy_namespaces = params[:should_provide_require_soy_namespaces] || false
      should_generate_goog_msg_defs = params[:should_generate_goog_msg_defs] || false
      use_goog_is_rtl_for_bidi_global_dir = params[:use_goog_is_rtl_for_bidi_global_dir] || false
      locales = params[:locales] || []
      message_file_path_format = params[:message_file_path_format] || false

      command = [ "java", "-jar", closure_templates_jar ]

      if should_generate_jsdoc
        command << "--shouldGenerateJsdoc"
      end

      if should_provide_require_soy_namespaces
        command << "--shouldProvideRequireSoyNamespaces"
      end

      if should_generate_goog_msg_defs
        command << "--shouldGenerateGoogMsgDefs"
      end

      if use_goog_is_rtl_for_bidi_global_dir
        command << "--useGoogIsRtlForBidiGlobalDir"
      end

      command << "--locales" << locales.join(',') unless ( not locales or locales.empty? )

      command << "--messageFilePathFormat" << message_file_path_format unless not message_file_path_format or message_file_path_format.empty?

      command << "--outputPathFormat" << output_path_format

      source.each do |file|
        command << file
      end

      when_writing "Running Closure Templates" do
        system(*command)
      end
    end

    def closure_templates_msg_extract(params)
      source = params[:source] || raise(ArgumentError, "source parameter required")
      output_file = params[:output_file] || false
      output_path_format = params[:output_path_format] || false
      input_prefix = params[:input_prefix] || false
      source_locale_string = params[:source_locale_string] || false
      target_locale_string = params[:target_locale_string] || false

      if not output_file || output_path_format
        raise(ArgumentError, "output_file or output_path_format parameter required")
      end

      command = [ "java", "-jar", closure_templates_msg_extractor_jar ]

      if input_prefix
        command << "--inputPrefix" << input_prefix
      end

      if source_locale_string
        command << "--sourceLocaleString" << source_locale_string
      end

      if target_locale_string
        command << "--targetLocaleString" << target_locale_string
      end

      if output_file
        command << "--outputFile" << output_file
      end

      if output_path_format
        command << "--outputPathFormat" << output_path_format
      end

      source.each do |source_file|
        command << source_file
      end

      when_writing "Running Closure Templates Msg Extractor" do
        system(*command)
      end
    end

    def closure_stylesheets_build(params)
      source = params[:source] || raise(ArgumentError, "source param required")
      target = params[:target] || raise(ArgumentError, "target param required")
      pretty_print = params[:pretty_print]
      allow_unrecognized_properties = params[:allow_unrecognized_properties]
      allow_properties = params[:allow_properties]
      allow_unrecognized_functions = params[:allow_unrecognized_functions]
      allow_functions = params[:allow_functions]
      defines = params[:defines]

      command = [ "java", "-jar", closure_stylesheets_jar ]

      command << "--pretty-print" if pretty_print

      if defines
        defines.each do |define|
          command << "--define" << define
        end
      end

      command << "--allow-unrecognized-properties" if allow_unrecognized_properties

      command << "--allow-unrecognized-functions" if allow_unrecognized_functions

      if allow_properties
        allow_properties.each do |property|
          command << "--allowed-unrecognized-property" << property
        end
      end

      if allow_functions
        allow_functions.each do |function|
          command << "--allowed-non-standard-function" << function
        end
      end

      command << source

      command << "--output-file" << target

      when_writing "Running Closure Stylesheets" do
        system(*command)
      end
    end

    def closure_compiler_build(files, params)
      command_line = ['java', '-jar', closure_compiler_jar]

      params = params || {}
      params.each_pair do |key, value|
        command_line << "--#{key.to_s.downcase}"
        command_line << value.to_s unless value.nil?
      end

      files.each do |file|
        command_line << file
      end

      when_writing "Compiling with: #{command_line}" do
        system(*command_line)
      end
    end

    def closure_build(params)
      roots = params[:roots] || nil
      files = params[:files] || nil
      inputs = params[:inputs] || nil
      namespaces = params[:namespaces] || nil

      if (not inputs) and (not namespaces)
        raise raise(ArgumentError, "inputs or namespaces parameter required")
      end

      defines = params[:defines] || []
      externs = params[:externs] || []
      debug = params[:debug] || false
      output_file = params[:output_file] || raise(ArgumentError, "output_file parameter required")
      output_mode = params[:output_mode] || "compiled"
      compiler_jar = params[:compiler_jar] || closure_compiler_jar
      compiler_summary_detail_level = params[:compiler_summary_detail_level] || 3
      compiler_warning_level = params[:compiler_warning_level] || "VERBOSE"
      compiler_compilation_level = params[:compiler_compilation_level] || "ADVANCED_OPTIMIZATIONS"
      compiler_process_closure_primitives = params[:compiler_process_closure_primitives] || true
      compiler_output_wrapper = params[:compiler_output_wrapper] || "(function(){%output%})();"
      compiler_warnings = params[:compiler_warnings]

      #using_closure_templates = params[:using_closure_templates]

      if not inputs.nil?
        files = ( files.nil? ? [] : files ) | inputs
      end

      command = [ "python", "#{closure_library_path}/closure/bin/build/closurebuilder.py" ]

      command << "--output_mode" << "#{output_mode}"

      command << "--compiler_jar" << "#{compiler_jar}"

      command << "--compiler_flags" << "--summary_detail_level=#{compiler_summary_detail_level}"

      command << "--compiler_flags" << "--warning_level=#{compiler_warning_level}"

      command << "--compiler_flags" << "--compilation_level=#{compiler_compilation_level}"

      command << "--compiler_flags" << "--output_wrapper=#{compiler_output_wrapper}"

      if compiler_process_closure_primitives
        command << "--compiler_flags" << "--process_closure_primitives"
      end

      if debug
        command << "--compiler_flags" << "--define=goog.DEBUG=true"
        command << "--compiler_flags" << "--debug"
        command << "--compiler_flags" << "--formatting=PRETTY_PRINT"
      else
        command << "--compiler_flags" << "--define=goog.DEBUG=false"
      end

      if defines
        defines.each do |name, value|
          command << "--compiler_flags" << "--define=#{name}=#{value}"
        end
      end

      if externs
        externs.each do |extern_file|
          command << "--compiler_flags" << "--externs=#{extern_file}" if extern_file
        end
      end

      if compiler_warnings
        compiler_warnings.each do |warning_code|
          command << "--compiler_flags" << "--jscomp_warning=#{warning_code}"
        end
      end

      #command << "--root" << "#{closure_library_path}/closure/goog"
      #command << "--root" << "#{closure_library_path}/third_party/closure"
      #command << "--root" << closure_templates_root_path if using_closure_templates

      if roots
        roots.each do |root_path|
          if root_path
            command << "--root" << root_path
          end
        end
      end

      if files
        files.each do |file|
          if file
            command << file
          end
        end
      end

      if namespaces
        namespaces.each do |namespace|
          if namespace
            command << "--namespace" << namespace
          end
        end
      end

      if inputs
        inputs.each do |input_file|
          if input_file
            command << "--input" << input_file
          end
        end
      end

      command << "--output_file" << output_file

      when_writing "Running Closure Builder" do
        system(*command)
      end
    end

    ## Task Generators
    def closure_generate_stylesheets_build_task(args)
      source = args[:source] || raise(ArgumentError, "source required")
      target = args[:target] || raise(ArgumentError, "target required")
      clean_mode = args[:clean_mode] || :clobber
      params = args[:params] || {}

      file source do |task|
        if not File.exists?(source)
          raise "El archivo #{source} ya debe existir"
        end
      end

      desc "Compila la hoja de estilo a partir de las fuentes GSS: #{source}"
      file target => source do |task|
        closure_build_stylesheet :source => source, :target => target
      end

      case clean_mode
      when :clean then
        CLEAN.include target
      when :clobber then
        CLOBBER.include target
      end

      return target
    end
  end
end
