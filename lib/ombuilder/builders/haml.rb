require "haml"
require "ombuilder/extensions/haml"
#require "ombuilder"

module OMBuilder
  module HamlBuilder
    include OMBuilder::BuilderBase unless include? OMBuilder::BuilderBase

    def haml(args)
      source = args[:source] || raise(ArgumentError, "source parameter required")
      target = args[:target] || raise(ArgumentError, "target parameter required")
      format = args[:format] || :html5
      cdata = args[:cdata] || false
      encoding = args[:encoding] || 'utf-8'
      locals = args[:locals] || {}

      haml_template = Haml::Engine.new(
        File.read(source),
        :filename => source,
        :format => format,
        :cdata => cdata,
        :encoding => encoding
      )

      log_command [ 'haml', source, target ]

      when_writing "Running HAML" do
        result = haml_template.render Haml::Context.new(File.dirname(source)), locals
        File.open(target, 'wb:utf-8') { |f| f.puts(result) }
      end
    end
  end
end
