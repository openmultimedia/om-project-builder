# encoding: utf-8
require 'ombuilder'
require "compass"
require "sass"
require "ombuilder/extensions/sass"

module OMBuilder
  module SassBuilder
    include OMBuilder::BuilderBase unless include? OMBuilder::BuilderBase

    def load_compass(sass_options, compass_options)
      compass_options = {} if compass_options.nil?

      Compass.add_project_configuration

      Compass.configuration.project_path = compass_options[:project_path] || Dir.pwd

      sass_options[:load_paths] += Compass.configuration.sass_load_paths
    end

    def sass(args)
      source = args[:source] || raise(ArgumentError, "source param required")
      target = args[:target] || raise(ArgumentError, "target param required")

      no_cache = args[:no_cache] || true
      debug = args[:debug] || false
      style = args[:style] || "nested"
      use_compass = args[:use_compass] || false
      load_paths = args[:load_paths] || []
      load_paths = [ load_paths ] if not load_paths.kind_of?(Array)
      params = args[ :params ] || []

      sass_options = {
        :filename => source,
        :cache => ! no_cache,
        :debug_info => debug,
        :style => style,
        :load_paths => load_paths,
        :custom => {
          :params => params
        }
      }

      compass_options = args[:compass_options]

      load_compass sass_options, compass_options if use_compass

      sass_template = Sass::Engine.for_file source, sass_options

      log_command ['sass', source, target]

      when_writing "Running SASS" do
        result = sass_template.render
        File.open(target, 'w:utf-8') { |f| f.puts(result) }
      end
    end

    alias scss sass
  end
end
