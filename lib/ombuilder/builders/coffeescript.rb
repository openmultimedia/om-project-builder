module OMBuilder
  module CoffeeScriptBuilder

    include BuilderBase unless include? BuilderBase

    def coffee(args)
      gemrequire 'coffee-script'

      source = args[:source]
      target = args[:target]

      log_command ['coffee', source, target]

      when_writing "Running CoffeeScript" do
        result = CoffeeScript.compile(File.open(source), {:bare => true})
        File.open(target, 'w:utf-8') { |f| f.puts(result) }
      end

    end
  end
end
