# encoding: utf-8
require 'ombuilder'
require 'ombuilder/builders/haml'
require 'ombuilder/builders/sass'
require 'ombuilder/builders/closure'
require 'rake/clean'

module OMBuilder
  module TaskBuilder
    include OMBuilder::BuilderBase unless include? OMBuilder::BuilderBase

    def batch_copy(args)

      from = args[:from] || nil

      to = args[:to] || raise(ArgumentError, "to required")

      files = args[:files] || raise(ArgumentError, "files required")

      clean_mode = args[:clean_mode] || :none

      copied_files = files.collect do |file|

        composite_source = from.nil? ? file : File.join(from, file)

        real_files = FileList[ composite_source ]

        real_files.collect do |source_file|
          if not from.nil?
            target_file = File.join to, source_file.sub(Regexp.new("^#{Regexp.escape(from)}"), "")
          else
            target_file = File.join to, source_file
          end

          target_dir = File.dirname target_file

          directory target_dir

          #desc "Copia (mediante hardlink) el archivo <#{source_file}> a #{target_file}"
          file target_file => [source_file, target_dir] do |task|
            rm target_file, :force => true
            cp source_file, target_file
          end

          if clean_mode == :clean
            CLEAN.include target_file
          elsif clean_mode == :clobber
            CLOBBER.include target_file
          end

          target_file
        end
      end

      copied_files.flatten
    end

    def import_files(args)
      import_map = args[:import_map] || raise(ArgumentError, "import_map required")
      source_path = args[:source_path] || "./"
      import_path = args[:import_path] || "./"
      clean_mode = args[:clean_mode] || :clean

      imported_files = FileList.new

      import_map.each_pair do |target, source|
        full_target = File.join(import_path, target)

        target_dir = File.dirname full_target

        directory target_dir

        ## desc "Importa el archivo <#{source}> a #{full_target}"
        file full_target => [source, target_dir] do |task|
          rm full_target, :force => true
          cp source, full_target
        end

        if clean_mode == :clean
          CLEAN.include full_target if CLEAN
        elsif clean_mode == :clobber
          CLOBBER.include full_target if CLOBBER
        end

        imported_files.add(full_target)
      end

      imported_files
    end
  end
end
