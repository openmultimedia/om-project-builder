require 'ombuilder/builders/haml'
require 'ombuilder/builders/closure'

module OMBuilder
  module ClosureTasksBuilder
    class HamlSoyBundle < Rake::TaskLib
      include ClosureBuilder unless include? ClosureBuilder
      include HamlBuilder unless include? HamlBuilder
      include SassBuilder  unless include? SassBuilder


      attr_accessor :path
      attr_accessor :target_soy_templates

      def built?
        @built
      end

      def initialize(path)
        @path = path

        @source_haml_templates = FileList[File.join(@path, '**/*.soy.haml')]

        @target_haml_templates = @source_haml_templates.collect do |haml_template|
          target = haml_template.ext_swap 'soy.haml', 'haml-out.soy'
          scss_source_file = haml_template.ext_swap 'soy.haml', 'scss'
          css_source_file = haml_template.ext_swap 'soy.haml', 'css'

          if File.exists? scss_source_file
            scss_target_file = scss_source_file.ext_swap 'scss', 'scss-out.css'

            file scss_target_file => scss_source_file do |t|
              scss :source => scss_source_file,
                :target => scss_target_file,
                :use_compass => true,
                :compass_options => {
                  :project_path =>
                    File.dirname(scss_source_file)
                }
            end

            CLEAN.include scss_target_file

            file target => scss_target_file

            css_target_file = scss_target_file

          elsif File.exists? css_source_file
            file target => css_source_file

            css_target_file = css_source_file
          end

          file target => haml_template do |f|
            haml :source => haml_template,
              :target => target,
              :locals => {
                :stylesheet_file => css_target_file
              }
          end

          target
        end

        CLEAN.include @target_haml_templates

        @source_soy_templates = @target_haml_templates | FileList[File.join(@path, '**/*.soy')]

        @target_soy_templates = @source_soy_templates.collect do |soy_template|
          target = soy_template.ext_swap 'soy', 'soy-out.js'

          file target => @source_soy_templates do |t|
            self.ensure_built
          end

          target
        end

        CLEAN.include @target_soy_templates
      end

      def ensure_built
        build unless built?
      end

      def build
        closure_templates_build(
          :source => @source_soy_templates,
          :output_path_format => "{INPUT_DIRECTORY}/{INPUT_FILE_NAME_NO_EXT}.soy-out.js",
          :should_generate_jsdoc => true,
          :should_provide_require_soy_namespaces => true,
          :should_generate_goog_msg_defs => true,
          :use_goog_is_rtl_for_bidi_global_dir => true
        )
      end
    end

    class << self
      def hamlsoy_bundles
        @hamlsoy_bundles ||= Hash.new
      end
    end

    include ClosureBuilder

    def hamlsoy_bundles
      OMBuilder::ClosureTasksBuilder.hamlsoy_bundles
    end

    def calculate_closure_files(*roots)
      FileList.new(roots.collect { |root| FileList[File.join(root, '**/*.js')] })
    end

    def hamlsoy_templates_task(path)
      return hamlsoy_bundles[path] if hamlsoy_bundles.key? path

      hamlsoy_bundles[path] = HamlSoyBundle.new path
    end

    def closure_prerequisites_task(*roots)
      dependencies = roots.collect do |root|
        hamlsoy_templates_task(root).target_soy_templates | calculate_closure_files(root)
      end

      dependencies.flatten
    end

    def closure_dependencies_task(args)
      roots = args[:roots].clone || Array.new
      files = args[:files].clone || Array.new

      raise "Roots or files required" if roots.empty? and files.empty?

      output_file = args[:output_file]

      raise "Output file required" if not output_file or output_file == ""

      clean_mode = args[:clean_mode] || :clean

      using_closure_library = args[:using_closure_library] || false
      using_closure_templates = args[:using_closure_templates] || false

      if using_closure_library
        roots <<
          closure_library_root_path <<
          closure_library_third_party_root_path
      end

      roots << closure_templates_root_path if using_closure_templates

      roots.uniq!
      files.uniq!

      dependencies = closure_prerequisites_task(*roots)

      file output_file => dependencies do |t|
        closure_deps :roots => roots,
          :files => files,
          :output_file => t.name
      end

      if clean_mode == :clean
        CLEAN.include output_file
      else
        CLOBBER.include output_file
      end
    end

    def closure_build_task(args)

      roots = args[:roots].clone || Array.new
      files = args[:files].clone || Array.new

      raise "Roots or files required" if roots.empty? and files.empty?

      inputs = args[:inputs].clone || Array.new
      namespaces = args[:namespaces].clone || Array.new

      raise "Inputs or namespaces required" if inputs.empty? and namespaces.empty?

      output_file = args[:output_file]

      raise "Output file required" if not output_file or output_file == ""

      externs = args[:externs] || Array.new
      defines = args[:defines] || Hash.new

      debug = args[:debug] || false

      clean_mode = args[:clean_mode] || :clean

      using_closure_library = args[:using_closure_library] || false
      using_closure_templates = args[:using_closure_templates] || false
      output_wrapper = args[:output_wrapper] || nil

      if using_closure_library
        roots <<
          closure_library_root_path <<
          closure_library_third_party_root_path
      end

      roots << closure_templates_root_path if using_closure_templates

      roots.uniq!
      files.uniq!

      dependencies = closure_prerequisites_task(*roots)

      file output_file => dependencies do |t|
        closure_build :roots => roots,
          :files => files,
          :inputs => inputs,
          :namespaces => namespaces,
          :externs => externs,
          :defines => defines,
          :debug => debug,
          :compiler_output_wrapper => output_wrapper,
          :output_file => t.name
      end

      if clean_mode == :clean
        CLEAN.include output_file
      else
        CLOBBER.include output_file
      end

    end

  end
end
