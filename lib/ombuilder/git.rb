module OMBuilder
  module GitManager
    def git?
      result = system 'git --version > /dev/null 2>&1'
      not result.nil?
    end
  end
end