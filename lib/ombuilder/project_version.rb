require 'yaml'

module OMBuilder
  class ProjectVersion
    attr_reader :major
    attr_reader :minor
    attr_reader :patch
    attr_reader :prerelease
    attr_reader :build

    def initialize(ver=nil)
      @major = 0
      @minor = 0
      @patch = 0
      @prerelease = []
      @build = []

      update ver unless ver.nil?
    end

    def self.from_file(version_file=".omversion")
      version = ProjectVersion.new
      if File.exists?(version_file)
        begin
          File.open(version_file, 'r' ) do |data|
            version.update YAML.load(data)
          end
        rescue Exception => e
          raise "Invalid version file: #{e}" 
        end
      end
      version
    end

    def to_s(type=:semver)
      type = :semver unless Set[:semver, :semver_tag, :gem].include? type

      case type 
      when :semver
        ver = "#{major}.#{minor}.#{patch}"
        ver << "-" << prerelease.join('.') unless prerelease.empty?
        ver << "+" << build.join('.') unless build.empty?
      when :semver_tag
        ver = 'v' << to_s(:semver)
      when :gem
        ver = "#{major}.#{minor}.#{patch}"
        ver << "." << prerelease.join('.') unless prerelease.empty?
        ver << "." << build.join('.') unless build.empty?
      end

      ver
    end

    def clone
      ProjectVersion.new to_hash
    end

    def ==(other)
      self.to_hash == other.to_hash
    end

    def to_hash
      return {
        :major => major,
        :minor => minor,
        :patch => patch,
        :prerelease => prerelease,
        :build => build
      }
    end

    def update(ver)
      hash_version = case ver
      when ProjectVersion then ver.to_hash
      when Hash then ver
      else
        begin
          parse_components(ver.to_s)
        rescue
          nil
        end
      end

      version_changed = true

      if (hash_version)
        @major = hash_version[:major] || 0
        @minor = hash_version[:minor] || 0
        @patch = hash_version[:patch] || 0
        @prerelease = hash_version[:prerelease] || []
        @build = hash_version[:build] || []
      else
        case ver
        when :major, "major"
          @major += 1
          @minor = 0
          @revision = 0
        when :minor, "minor"
          @minor += 1
          @patch = 0
        when :patch, "patch"
          @patch += 1
        else
          #Esta expresion permite asiganr prerelease y build vacios para eliminar esos datos: "-+debug", "-pre+" o "-+" para eliminar ambos
          special_regex = /^(\-([a-z0-9-]+(?:\.[a-z0-9-]+)*)?)?(\+([a-z0-9-]+(?:\.[a-z0-9-]+)*)?)?/i
          matches = special_regex.match ver

          if matches.nil? or matches[0].empty?
            version_changed = false
          else
            @prerelease = (matches[2].nil? ? [] : matches[2].split('.')) unless matches[1].nil?
            @build = (matches[4].nil? ? [] : matches[4].split('.')) unless matches[3].nil?  
          end
        end
      end
      version_changed
    end

    def parse_components(version)
      regex = /^v?(\d+)(?:\.(\d+)(?:\.(\d+))?)?(?:\-(?=[a-z0-9])([a-z0-9-]+(?:\.[a-z0-9-]+)*)?)?(?:\+(?=[a-z0-9])([a-z0-9-]+(?:\.[a-z0-9-]+)*)?)?/i
      matches = regex.match version

      if (matches.nil?)
        raise 'Version #{version} unparsable'
      end

      ver = {
        :major => matches[1].to_i,
        :minor => matches[2].to_i,
        :patch => matches[3].to_i
      }

      if not matches[4].nil?
        ver[:prerelease] = matches[4].split('.')
      end

      if not matches[5].nil?
        ver[:build] = matches[5].split('.')
      end

      ver
    end
  end
end