require 'ombuilder/project_version'

module OMBuilder
  VERSION = ProjectVersion::from_file.to_s :gem
end