require 'rake/tasklib'
require 'ombuilder/project_base'

module OMBuilder
  class ProjectConfiguration < Rake::TaskLib
    include ProjectBase

    attr_reader :project

    def initialize(name, project, &block)
      @project = project
      super(name, &block)
    end

    def output_path
      return File.join(project.output_path, name.to_s)
    end
  end
end