# -- encoding: utf-8 --
require 'rake'
require 'ombuilder/project'
require 'ombuilder/git'
require 'yaml'

class OMBuilder::VersionTask < Rake::TaskLib
  include OMBuilder::BuilderBase
  include OMBuilder::GitManager

  attr_accessor :version_file
  attr_reader :version

  def initialize
    @projects = Array.new
    @version_file = '.omversion'

    yield(self) if block_given?

    if File.exists?(@version_file)
      @version = OMBuilder::ProjectVersion.from_file @version_file
    else
      @version = OMBuilder::ProjectVersion.new
    end
    
    define_tasks
  end

  def to_s(format=nil)
    @version.to_s(format)
  end

  def add_project(*projects)
    for proj in projects
      @projects << proj
    end

    proj.version.update @version
  end

  def define_tasks
    
    desc "Actualiza la versión del proyecto y crea un tag en el repositorio git. 
      Se puede utilizar :nobump como version si sólo se requiere crear el tag
      correspondiente"

    task :version, [:version, :tag] do |t, args|
      new_version = args[:version]
      tag = args[:tag?].nil? ? false: case args[:tag?].downcase when 'true', 't', 'yes' then true else false end

      notice "Current version: #{version.to_s}"
      
      if not new_version.nil?
        if (tag)
          changes = `git status --porcelain`

          if changes.nil? or not changes.empty?
            raise "El repositorio no esta limpio"
          end
        end

        bumped = @version.update new_version

        File.open(@version_file, "w") do |file|  
          YAML.dump(version.to_hash, file)
        end

        if tag
          if bumped
            system('git', 'add', @version_file)
            system('git', 'commit', '-m', "Version bump to #{@version.to_s :semver_tag}")
          end

          system('git', 'tag', '-s', @version.to_s)
        end

        notice "Project version updated to: %s" % [@version]
      end
    end
  end
end