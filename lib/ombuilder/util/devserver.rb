require 'rake'
require 'sinatra/base'
require 'net/http'
require 'uri'

module OMBuilder
  class StaticServer < Sinatra::Base

    ENV_INCLUDE_REQUEST_HEADERS = Set['CONTENT_TYPE', 'CONTENT_LENGTH' ]
    ENV_INCLUDE_RESPONSE_HEADERS = Set['Content-Type', 'Etag', 'Last-Modified', 'Content-Length' ]

    def self.set_proxy(path,url)

      [:get, :post, :delete].each do |method|

        self.send method, Regexp.new("#{path}(/.*)?") do |subpath|

          uri = URI(url + ( if subpath.nil? then '' else subpath end ) )

          new_uri = uri.class.build :host => uri.host,
            :path => uri.path,
            :query => request.query_string

          Net::HTTP.start(new_uri.host, new_uri.port) do |http|
            fwd_request_headers = env.inject({}) do |acc, (k,v)|
              if (k =~ /^http_(.*)/i or ENV_INCLUDE_REQUEST_HEADERS.include? k)
                acc[($1 || k).downcase.gsub('_','-')] = v
              end

              acc
            end

            case method
            when :get
              fwd_request = Net::HTTP::Get.new new_uri.to_s, fwd_request_headers

            when :delete
              fwd_request = Net::HTTP::Delete.new new_uri.to_s, fwd_request_headers

            when :post then
              fwd_request = Net::HTTP::Post.new new_uri.to_s, fwd_request_headers
              fwd_request.body_stream = request.body
            end

            fwd_response = http.request fwd_request

            fwd_response_headers = {}
            fwd_response.each_capitalized do |key,value|

              if ENV_INCLUDE_RESPONSE_HEADERS.include?(key) then
                # puts "Including header: #{key} = #{value}"
                fwd_response_headers[key] = value
              else
                # puts "Ignoring header: #{key} = #{value}"
              end

            end

            status fwd_response.code
            headers fwd_response_headers
            body fwd_response.body
          end

        end

      end

    end

    configure :production, :development do
      enable :logging
    end

    set :public_folder, '.'

    set :bind, '0.0.0.0'

    not_found do
      path = File.join(settings.public_dir, request.path)

      if File.directory? path
        if (request.path[-1..-1] != '/')
          redirect request.path + '/'
        else
          Dir.entries(path).sort.map { |e| "<li><a href=\"#{e}\">#{e}</a></li>" }
        end
      else
        pass
      end
    end
  end
end
