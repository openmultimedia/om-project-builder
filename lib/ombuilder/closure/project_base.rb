module OMBuilder
  module ClosureProjectBase
    attr_reader :roots
    attr_reader :files
    attr_reader :inputs
    attr_reader :namespaces
    attr_reader :externs
    attr_reader :defines
    attr_reader :externs_path

    def initialize(*args, &block)
      @roots = []
      @files = []
      @inputs = []
      @namespaces = []
      @externs = []
      @defines = {}

      @externs_path = Array.new

      super(*args, &block)
    end

    def add_root(*path)
      path.each do |single_path|
        @roots << relative_path_to(single_path)
      end
    end

    def add_file(*file)
      file.each do |single_path|
        @files << relative_path_to(single_path)
      end
    end

    def add_input(*file)
      file.each do |single_path|
        @inputs << relative_path_to(single_path)
      end
    end

    def add_namespace(*namespace)
      namespace.each do |single_ns|
        @namespaces << single_ns
      end
    end

    def add_extern(*extern)
      extern.each do |single_extern|
        @externs << single_extern
      end
    end

    def add_define(defines_hash)
      @defines.merge! defines_hash
    end

    def add_externs_path(externs_path)
      @externs_path << externs_path
    end
  end
end