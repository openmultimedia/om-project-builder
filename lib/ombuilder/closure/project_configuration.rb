require 'ombuilder/project_configuration'
require 'ombuilder/closure/project_base'
require 'ombuilder/builders/closure_tasks'

module OMBuilder
  class ClosureProjectConfiguration < OMBuilder::ProjectConfiguration
    include ClosureProjectBase
    include ClosureBuilder
    include ClosureTasksBuilder

    def devel=(val)
      @devel = val
    end

    alias :set_devel :devel=

    def devel?()
      @devel
    end

    def debug=(val)
      @debug = val
    end

    alias :set_debug :debug=

    def debug?()
      @debug
    end

    attr_writer :output_file
    
    alias :set_output_file :output_file=

    attr_accessor :output_wrapper

    alias :set_output_wrapper :output_wrapper=

    def output_file
      @output_file || (devel? ? 'deps.js' : 'compiled.js')
    end

    def output_file_path
      File.join(output_path, output_file)
    end

    def full_roots
      project.roots | roots
    end

    def full_inputs
      project.inputs | inputs
    end

    def full_files
      project.files | files | full_inputs
    end

    def full_namespaces
      project.namespaces | namespaces
    end

    def full_externs
      project.externs | externs
    end

    def full_externs_path
      project.externs_path | externs_path
    end

    def quoted_defines
      returned_quoted_defines = Hash.new
      
      project.defines.merge(defines).each do |key, value|
        returned_quoted_defines[key] = case value
          when NilClass then "true"
          when Integer then "#{value}"
          when TrueClass then "true"
          when FalseClass then "false"
          else "'#{value}'"
        end
      end

      returned_quoted_defines
    end

    def resolved_externs
      matched_externs = Hash.new

      full_externs.each do |extern|
        matched_externs[extern] = 
          File.exists?(resolved_path = relative_path_to(extern)) ?
          resolved_path :
          nil
      end

      full_externs_path.each do |path|
        next unless File.directory? path
          
        matched_externs.each_key do |extern_name|
          next unless matched_externs[extern_name].nil?

          extern_file = Dir.entries(path).find do |file|
            Regexp.new('^' + extern_name + '(-\d+(\.\d)*)?\.externs\.js$', Regexp::IGNORECASE) =~ file
          end

          matched_externs[extern_name] = File.join path, extern_file unless extern_file.nil?
        end
      end

      matched_externs.each_key do |extern_name|
        raise "No existe un extern que coincida con: #{extern_name}" if matched_externs[extern_name].nil?
      end

      return matched_externs.values.uniq      
    end

    def initialize(name, project, &block)
      @devel = false
      @debug = false

      super(name, project, &block)
    end

    def define_tasks
      directory output_path

      file output_file_path => output_path

      if devel?        
        
        closure_dependencies_task :roots => full_roots,
          :files => full_files,
          :using_closure_library => true,
          :using_closure_templates => true,
          :output_file => output_file_path

      else
        
        closure_build_task :roots => full_roots,
          :files => full_files,
          :inputs => full_inputs,
          :namespaces => full_namespaces,
          :defines => quoted_defines,
          :externs => resolved_externs,
          :debug => debug?,
          :using_closure_library => true,
          :using_closure_templates => true,
          :output_wrapper => output_wrapper,
          :output_file => output_file_path
      end

      super     

      task :default => output_file_path
    end
  end
end