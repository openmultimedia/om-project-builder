require 'ombuilder/project'
require 'ombuilder/builders/closure'
require 'ombuilder/builders/task'
require 'ombuilder/closure/project_base'
require 'ombuilder/closure/project_configuration'

module OMBuilder
  class ClosureProject < OMBuilder::Project
    include ClosureProjectBase

    def configuration_class
      ClosureProjectConfiguration
    end
  end
end