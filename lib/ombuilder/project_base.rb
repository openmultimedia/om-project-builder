require 'ombuilder'

module OMBuilder
  module ProjectBase
    include OMBuilder::BuilderBase

    attr_reader :name
    attr_reader :tasks
    attr_accessor :output_path

    alias :set_output_path :output_path=

    def initialize(name, &block)
      @name = name
      @included_tasks = Array.new
      @tasks = Hash.new
      @output_path = './build/'

      @namespace = namespace name do end

      namespace name do
        instance_eval &block if block_given?
        define_tasks
      end
    end

    def include_task(&block)
      @included_tasks << block
    end

    def define_tasks
      directory output_path
      CLOBBER.include output_path

      @included_tasks.each do |task_block|
        self.instance_eval &task_block
      end
    end

    def [](task_name)
      @namespace[task_name]
    end
  end
end
