[
  "context",
  "filters",
  "buffer_css",
  "engine_css",
  "engine_soy",
  "helpers"
].each { |lib| require "ombuilder/extensions/haml/#{lib}" }
