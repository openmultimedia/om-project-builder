class String
  def ext_swap(new_or_old_ext, new_ext=nil)
    old_ext = new_or_old_ext unless new_ext.nil?
    new_ext = new_or_old_ext if new_ext.nil?

    raise "The old extension cannot be empty" if old_ext and old_ext == ''

    return self.dup if ['.', '..'].include? self

    if new_ext != ''
      new_ext = (new_ext =~ /^\./) ? new_ext : ("." + new_ext)
    end

    if old_ext
      old_ext = (old_ext =~ /^\./) ? old_ext : ("." + old_ext)
    end

    if old_ext
      dup.sub!(Regexp.new('([^/\\\\])' + Regexp.escape(old_ext) + '$')) do
        $1 + new_ext
      end || self
    else
      dup.sub!(%r(([^/\\])\.[^./\\]*$)) { $1 + new_ext } || self + new_ext
    end
  end
end