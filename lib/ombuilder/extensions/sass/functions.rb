module Sass::Script::Functions
    def param(name)
        params = (options[:custom] && options[:custom][:params]) || {}
        
        val = params[name]
        
        if ( val.is_a? TrueClass )
            Sass::Script::Bool.new(true)
        elsif ( val.is_a? FalseClass )
            Sass::Script::Bool.new(false)
        else
            Sass::Script::String.new(val)
        end
    end

    declare :param, [:name]
end
