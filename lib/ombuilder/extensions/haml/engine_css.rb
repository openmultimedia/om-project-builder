class Haml::Engine
  module Css
    class StyleProvider
      INLINE = 0
      CLASS = 1

      def initialize(cssFile=nil)
        require 'rubygems'
        #require 'css_parser'
        require 'csspool'

        @mode = INLINE

        #@parser = CssParser::Parser.new
        @doc = nil

        load! cssFile if ( cssFile )
      end

      def load!(cssFile)
        if ( cssFile and not cssFile.nil? and not cssFile.empty? )
            #@parser.load_file! cssFile
            File.open cssFile do |content|
              @css_doc = CSSPool.CSS content
            end
        end
      end

      def [](*selectors)
        return '' if @css_doc.nil?

        collected = selectors.collect do |selector|
          @css_doc[selector].collect do |rs| 
            rs.declarations
          end

          #@parser.find_by_selector(selector)
        end

        return collected.flatten.join
      end
    end
  end
end
