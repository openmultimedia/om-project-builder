require 'haml'

class Haml::Context
  def init_tab_tracking
    return if @tracking

    @classes = Array.new

    @haml_buffer.listen :tabulation do
      @classes = @classes.slice 0, @haml_buffer.tabulation
      @classes[@haml_buffer.tabulation] = ''
    end

    @tracking = true
  end

  def basedir()
    return @basedir_stack.length > 0 ? @basedir_stack[0] : ''
  end

  def soy
    @soy ||= Haml::Engine::Soy.new @haml_buffer
  end

  def style
    @style_provider
  end

  def use_stylesheet(file, use_tracking = false)
    if ( ! @style_provider )
      @style_provider = Haml::Engine::Css::StyleProvider.new file
    else
      @style_provider.load! file
    end

    init_tab_tracking if use_tracking
  end

  def css(selector, extras = '')
    selector = selector.instance_of?(Symbol) ? "." << selector.to_s : selector.to_s

    if @tracking
      @classes[@haml_buffer.tabulation] = selector

      selector = @classes.join ' '
    end

    style = (@style_provider ? @style_provider[selector] : '')

    return :style  => "#{style}#{style.length > 0 ? ' ': ''}#{extras}"
  end

  def render_partial(source, locals = {})
    source = File.expand_path(source, basedir)

    if not File.exists? source
      template_source = "[Partial #{source} not found]"
      raise template_source
    else
      template_source = File.read(source);
    end

    template = Haml::Engine::new(
      template_source,
      :filename => source,
      :format => :html5,
      :escape_attrs => false
    )

    @basedir_stack.unshift(File.dirname(source))
    ret = template.render(self, locals)
    @basedir_stack.shift

    ret
  end

  def dir(path)
    dir_path = File.expand_path(path, basedir)
    return Dir[dir_path].sort
  end

  def initialize(basedir)
    @basedir_stack = [ basedir ]
  end
end
