require 'haml/filters'

module Haml::Filters::SoyDoc
  include Haml::Filters::Base

  def render(text)
    "/**\n" + text.gsub(/^/m, " * ") + " */\n"
  end
end
