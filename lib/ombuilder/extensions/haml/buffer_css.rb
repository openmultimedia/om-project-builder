class Haml::Buffer
  def listen(event, &callback)
    @callbacks = {} if @callbacks.nil?

    @callbacks[event] = Array.new if @callbacks[event].nil?

    @callbacks[event].push callback;
  end

  def on_tabulation_changed
    if not @callbacks.nil?
      callbacks = @callbacks[:tabulation]
      if not callbacks.nil? and not callbacks.empty?
        callbacks.each do |callback|
          callback.call
        end
      end
    end
  end

  original_push_text = instance_method(:push_text)

  define_method(:push_text) do |text, tab_change, dont_tab_up|
    original_push_text.bind(self).call(text, tab_change, dont_tab_up)

    on_tabulation_changed
  end

  original_adjust_tabs = instance_method(:adjust_tabs)

  define_method(:adjust_tabs) do |tab_change|
    original_adjust_tabs.bind(self).call(tab_change)

    on_tabulation_changed
  end
end
