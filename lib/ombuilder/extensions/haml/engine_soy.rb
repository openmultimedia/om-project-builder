class Haml::Engine
  class Soy
    def initialize(buffer)
      @buffer = buffer
    end

    def template(name)
      @buffer.push_text "{template .#{name}}\n", 1, true
      yield if block_given?
      @buffer.push_text "{/template}\n", -1, true
    end

    def namespace(namespace)
      @buffer.push_text "{namespace #{namespace}}\n", 0, false
    end

    def if(conditions)
      @buffer.push_text "{if #{conditions}}\n", 1, true
      yield if block_given?
      @buffer.push_text "{/if}\n", -1, true
    end

    def foreach(conditions)
      @buffer.push_text "{foreach #{conditions}}\n", 1, true
      yield if block_given?
      @buffer.push_text "{/foreach}\n", -1, true
    end

    def ifempty
      @buffer.push_text "{ifempty}\n", 0, true
      yield if block_given?
    end

    def Soy.print(text)
      "{#{text}}"
    end

    def Soy.css(class_name)
      "{css #{class_name}}"
    end

    def Soy.namespace(namespace)
      "{namespace #{namespace}}"
    end

    class CssClassChain
      @haml_buffer = nil
      @class_chain = nil

      def initialize(haml_buffer)
        @haml_buffer = haml_buffer
        @class_chain = Array.new
      end

      def make
        (@class_chain.select {|item| item } ).join("-")
      end

      def set(class_suffix)

        class_suffix = class_suffix.to_s.strip

        if class_suffix.empty? then
          @class_chain[ @haml_buffer.tabulation ] = nil
        else
          @class_chain[ @haml_buffer.tabulation ] = class_suffix
        end

        @class_chain = @class_chain.slice(0, @haml_buffer.tabulation + 1)
      end

      def get(class_suffix, extra = nil)
        set(class_suffix)
        if extra then
          {:class => Soy.css(make << "-" << extra)}
        else
          {:class => Soy.css(make)}
        end
      end
    end
  end
end
