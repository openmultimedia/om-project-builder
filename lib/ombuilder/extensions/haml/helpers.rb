
require 'haml/helpers'

module Haml::Helpers
  def format_code(input)
    preserve(html_escape(input))
  end
end
