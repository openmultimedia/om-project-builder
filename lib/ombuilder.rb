# coding: utf-8
require "pathname"
require "ombuilder/extensions/string"
require "rake"

module OMBuilder
  module BuilderBase
    include Rake::DSL unless include? Rake::DSL

    def relative_path_to(to_path, from_path='./')
      to_path_expanded = File.expand_path(to_path) 
      from_path_expanded = File.expand_path(from_path) 

      #TODO: Validar cuando el archivo existe
      if not from_path =~ %r{/$} and not File.directory? from_path_expanded
        from_path_expanded.gsub! %r{/[^/\\]+$}, '/' 
      end

      relative_path = Pathname.new(to_path_expanded).
        relative_path_from(Pathname.new from_path_expanded).
        to_s

      relative_path << "/" if to_path.match(%r{/$})
        
      relative_path
    end

    def notice(*args)
      cadenas = args.collect { |arg| "INFO: " << arg }
      puts(*cadenas)
    end

    def log(*args)
      return if not verbose
      puts(*args)
    end

    def log_command(*command)
      log( (command.collect { |i| i.include?(" ") ? %Q{"#{i}"} : ((i.nil? || i.empty?) ? '<!?>' : i) }).join(" ") )
    end

    def system(*command)
      log_command command
      success = Kernel::system(*command)
      raise Exception.new("Error al ejecutar el comando externo") if not success
      success
    end
  end
end