require "spec_helper"
require "ombuilder"

describe OMBuilder do
  let (:dbld) do
    class DummyBuilderClass 
      include OMBuilder::BuilderBase
    end

    DummyBuilderClass.new
  end

  describe ".relative_path_to(to,from=pwd)" do

    context "From parent path" do

      example "should return a path not starting with slashes or dots" do

        dbld.relative_path_to("/long/path/subpath/file.ext", "/long/path/").
          should eq "subpath/file.ext"

        dbld.relative_path_to("/long/path/subpath/", "/long/path/").
          should eq "subpath/"

        dbld.relative_path_to("/long/path/subpath/file.ext", "/long/path/otherfile.ext").
          should eq "subpath/file.ext"

      end

    end

    context "using a subpath of a path" do
      example "should return a relative path including dots" do

        dbld.relative_path_to("/long/path/", "/long/path/subpath/").
          should eq "../"

        dbld.relative_path_to("/long/path/", "/long/path/subpath/longer/").
          should eq "../../"

      end
    end

    context "using a common parent path" do
      example "should return a relative path including dots" do

        dbld.relative_path_to("/common/path/other/path", "/common/path/different/path/").
          should eq "../../other/path"

        dbld.relative_path_to("/long/path/", "/long/path/subpath/longer/").
          should eq "../../"

      end
    end

    context "using a subpath of a path" do
      example "should return a relative path including dots" do

        dbld.relative_path_to("/long/path/", "/long/path/subpath/").
          should eq "../"

        dbld.relative_path_to("/long/path/", "/long/path/subpath/longer/").
          should eq "../../"
      end
    end
  end

  describe ".notice(*args)" do
    
    example do
      dbld.
        should respond_to :notice
    end

  end

  describe ".log(*args)" do
    
    example do
      dbld.
        should respond_to :log
    end

  end

  describe ".log_command(*args)" do

    example do
      dbld.
        should respond_to :log_command
    end

  end

  describe ".system(*args)" do
    
    example do
      dbld.
        should respond_to :system
    end

  end

end

describe String do
  describe ".ext_swap" do

    let (:origin_filename) { "source_file.origin" }
    let (:target_filename) { "source_file.target" }

    let (:origin_filename_complex) { "source_file.other.origin" }
    let (:target_filename_simple) { "source_file.other.target" }
    let (:target_filename_complex) { "source_file.target.other" }

    example "must return the extension changed" do
      origin_filename.ext_swap('target').
        should eq target_filename

      origin_filename.ext_swap('origin', 'target').
        should eq target_filename

      origin_filename.ext_swap('other', 'target').
        should eq origin_filename

      lambda  { origin_filename.ext_swap('', 'target') }.
        should raise_error "The old extension cannot be empty"

      origin_filename_complex.ext_swap('target').
        should eq target_filename_simple

      origin_filename_complex.ext_swap('origin', 'target').
        should eq target_filename_simple

      origin_filename_complex.ext_swap('other.origin', 'other.target').
        should eq target_filename_simple

      origin_filename_complex.ext_swap('other.origin', 'target').
        should eq target_filename

      origin_filename_complex.ext_swap('other.origin', 'target.other').
        should eq target_filename_complex
    end

  end
end